//from https://stackoverflow.com/questions/64722413/force-webpage-dom-wait-on-promise

```js
	initPage();

	async function initPage() {
		console.log('initializing page...');
		const wasmExports = await initWasm();
		renderPageContent(wasmExports);
	}

	// returns Promise<WasmModuleExports>
	async function initWasm() {
		const { instance } = await WebAssembly.instantiate(array, imports);
		return instance;
	}

	function renderPageContent(lwasmExports) {
		//Render a green square (we don't actually see this)
		wgl.clearColor( 0.,1.,0.,1. ); 
		wgl.clear( wgl.COLOR_BUFFER_BIT | wgl.COLOR_DEPTH_BIT );

		instance = lwasmExports;
		wasmExports = instance.exports;
		instance.exports.main();
		console.log('✓ page content rendered');
	}

	//This code executes BEFORE "✓ page content rendered"
	console.log( "should happen after initPage()" );

	//Render a red box.
	wgl.clearColor( 1.,0.,0.,1. ); 
	wgl.clear( wgl.COLOR_BUFFER_BIT | wgl.COLOR_DEPTH_BIT );
	console.log( "rendering red complete\n" );
	//After this function completes, the HTML document is rendered with the red box.
```

Outputs:

```
index.htlm:495 initializing page...
index.html:518 should happen after initPage()
index.html:523 rendering red complete
index.html:477 Main started.  This will appear in your browser console.
index.html:514 ✓ page content rendered
```
